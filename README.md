This repository is used as the entry point for fetching the Yocto BSP sources
using the `repo` command-line tool.

Quick usage:

* Using `repo`, download the XML manifest from this repository:

```
$ repo init -u git@bitbucket.org:mmx-mase-pub/yocto-repo-manifest.git
```

* Once downloaded, you can fetch all referenced repositories:

```
$ repo sync
```

* Finally, you can start a BSP/SDK build using:

```
$ cd yocto-build/
$ ./build.sh -v [options]
```

See the README in the `yocto-build` repository for more information.

Repo can be downloaded at https://gerrit.googlesource.com/git-repo/
